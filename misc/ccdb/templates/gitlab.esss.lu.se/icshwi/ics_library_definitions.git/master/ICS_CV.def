###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_CV                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                                  CV - Control valve                                      ##
##                                                                                          ##  
##                                                                                          ##  
############################           Version: 1.4           ################################
# Author:	Miklos Boros 
# Date:		28-04-2020
# Version:  v1.4
# Changes:
# SetpointLimitation and SS
############################           Version: 1.3           ################################
# Author:	Miklos Boros 
# Date:		27-05-2019
# Version:  v1.3
# Changes:
# Variable name unification
############################           Version: 1.2           ################################
# Author:	Miklos Boros 
# Date:		28-02-2019
# Version:  v1.2
# Changes:
# 1. Changed smoother ramping
############################           Version: 1.1           ################################
# Author:	Miklos Boros, Marino Vojneski 
# Date:		12-06-2018
# Version:  v1.1
# Changes:
# 1. Modified Alarm Signal section to be compatible with new format.
############################           Version: 1.0           ################################
# Author:	Miklos Boros 
# Date:		08-01-2018
# Version:  v1.0
##############################################################################################



############################
#  STATUS BLOCK
############################ 
define_status_block()


#Operation modes
add_digital("OpMode_Auto",                           PV_DESC="Operation Mode Auto",        PV_ONAM="True",             PV_ZNAM="False")      
add_digital("OpMode_Manual",                         PV_DESC="Operation Mode Manual",      PV_ONAM="True",             PV_ZNAM="False")
add_digital("OpMode_Forced",                         PV_DESC="Operation Mode Forced",      PV_ONAM="True",             PV_ZNAM="False")
add_analog("ValveColor","INT",                       PV_DESC="BlockIcon valve color")


#Valve states
add_analog("ValvePosition","REAL",  ARCHIVE=True,    PV_DESC="Valve Position AI",          PV_EGU="%")
add_analog("ValveSP","REAL",  ARCHIVE=True,          PV_DESC="Valve Setpoint",             PV_EGU="%")
add_analog("ValveMV","REAL",  ARCHIVE=True,          PV_DESC="Valve Control AO",           PV_EGU="%")
add_digital("Opening",                               PV_DESC="Valve opening",              PV_ONAM="Opening",          PV_ZNAM="NotMoving")
add_digital("Closing",                               PV_DESC="Valve closing",              PV_ONAM="Closing",          PV_ZNAM="NotMoving")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",                        PV_DESC="Inhibit Manual Mode",        PV_ONAM="InhibitManual",    PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",                         PV_DESC="Inhibit Force Mode",         PV_ONAM="InhibitForce",     PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",                          PV_DESC="Inhibit Locking",            PV_ONAM="InhibitLocking",   PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("GroupInterlock",                        PV_DESC="Group Interlock",            PV_ONAM="True",             PV_ZNAM="False")
add_string("InterlockMsg", 39,                       PV_NAME="InterlockMsg",               PV_DESC="Interlock Message")
add_digital("MoveInterlock",  ARCHIVE=True,          PV_DESC="Move Interlock",             PV_ONAM="True",             PV_ZNAM="False")

#for OPI visualization
add_digital("EnableAutoBtn",                         PV_DESC="Enable Auto Button",         PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableManualBtn",                       PV_DESC="Enable Manual Button",       PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableForcedBtn",                       PV_DESC="Enable Force Button",        PV_ONAM="True",             PV_ZNAM="False")

#Block Icon controls
add_digital("EnableBlkCtrl",                         PV_DESC="Enable Block SP Button",     PV_ONAM="True",             PV_ZNAM="False")

#Forcing
add_digital("EnableForceValBtn",                     PV_DESC="Enable Force Value Button",  PV_ONAM="True",             PV_ZNAM="False")

#Locking mechanism
add_digital("DevLocked",                             PV_DESC="Device locked",              PV_ONAM="True",             PV_ZNAM="False")
add_analog("Faceplate_LockID","DINT",                PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID","DINT",                PV_DESC="Guest Lock ID")

add_digital("LatchAlarm",                            PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                            PV_DESC="Group Alarm for OPI")

#Alarm signals
add_major_alarm("Position_Discrep","Position Discrepancy",           PV_ZNAM="NominalState")
add_major_alarm("IO_Error","HW IO Error",                            PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error","HW Input Module Error",        PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error","HW Output Module Error",      PV_ZNAM="NominalState")
add_minor_alarm("SPLimitActive","SPLimitActive",                     PV_ZNAM="NominalState")
add_major_alarm("SSTriggered","SSTriggered",                         PV_ZNAM="NominalState")
add_major_alarm("StaPnR","Status power and ready",                   PV_ZNAM="NominalState")
add_major_alarm("Hi","Status Hi",                   PV_ZNAM="NominalState")
add_major_alarm("Lo","Status Lo",                   PV_ZNAM="NominalState")

#Discrepancy
add_analog("DiscrPerc","REAL" ,                      PV_DESC="Discrepancy In Percent",                     PV_EGU="%")
add_time("DiscrTime",                                PV_DESC="Discrepancy Time Interval")

#Ramping
add_analog("MaxRampUPSpd","REAL",                    PV_DESC="Maximum Ramping UP Speed",                 PV_EGU="%/s")
add_analog("MaxRampDNSpd","REAL",                    PV_DESC="Maximum Ramping DOWN Speed",               PV_EGU="%/s")
add_analog("ActRampSpeed","REAL",                    PV_DESC="Actual Ramping Speed",                     PV_EGU="%/s")
add_digital("Ramping",                               PV_DESC="Ramping Indicator",                        PV_ONAM="True",                                     PV_ZNAM="False")
add_digital("RampSettingOK",                         PV_DESC="Ramping can be enabled",                   PV_ONAM="True",                                     PV_ZNAM="False")

add_analog("FB_RampUPTIME","INT",                    PV_DESC="Ramping UP time",                          PV_EGU="sec")
add_analog("FB_RampUPRANGE","REAL",                  PV_DESC="Ramping UP range",                         PV_EGU="%")
add_analog("FB_RampDNTIME","INT",                    PV_DESC="Ramping DOWN time",                        PV_EGU="sec")
add_analog("FB_RampDNRANGE","REAL",                  PV_DESC="Ramping DOWN range",                       PV_EGU="%")


#Feedbacks
add_analog("FB_ForcePosition","REAL" ,               PV_DESC="FB Force Valve Position (AI)",             PV_EGU="%")
add_analog("FB_Setpoint","REAL" ,                    PV_DESC="FB Setpoint from HMI (SP)",                PV_EGU="%")
add_analog("FB_Manipulated","REAL" ,                 PV_DESC="FB Manipulated Value (AO)",                PV_EGU="%")
add_analog("FB_Step","REAL" ,                        PV_DESC="FB Step value for Open/Close",             PV_EGU="%")
add_analog("FB_RampTIME","INT" ,                     PV_DESC="Ramping Time",                             PV_EGU="sec")
add_analog("FB_RampRANGE","REAL" ,                   PV_DESC="Ramping Range",                            PV_EGU="%")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                              PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",                            PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",                             PV_DESC="CMD: Force Mode")

add_digital("Cmd_RampON",                            PV_DESC="Turn Ramping ON")
add_digital("Cmd_RampOFF",                           PV_DESC="Turn Ramping OFF")

add_digital("Cmd_AckAlarm",                          PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceValInp",                       PV_DESC="CMD: Force Valve PLC Input")
add_digital("Cmd_ForceValOut",                       PV_DESC="CMD: Force Valve PLC Output")

add_digital("Cmd_ForceUnlock",                       PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",                           PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",                         PV_DESC="CMD: Unlock Device")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Setpoint and Manipulated value from HMI
add_analog("P_ForcePosition","REAL" ,                PV_DESC="Force Valve position (AI)",             PV_EGU="%")
add_analog("P_Setpoint","REAL" ,                     PV_DESC="Setpoint from HMI (SP)",                PV_EGU="%")
add_analog("P_Manipulated","REAL" ,                  PV_DESC="Manipulated value (AO)",                PV_EGU="%")

#Step value when pressing Cmd_Open1Step or Cmd_Close1Step
add_analog("P_Step","REAL" ,                         PV_DESC="Step value for open close",             PV_EGU="%")

#Ramping speed from the HMI
add_analog("P_RampUPTIME","INT",                     PV_DESC="Ramping UP Time",                          PV_EGU="sec")
add_analog("P_RampUPRANGE","REAL",                   PV_DESC="Ramping UP Range",                         PV_EGU="%")
add_analog("P_RampDNTIME","INT",                     PV_DESC="Ramping DOWN Time",                        PV_EGU="sec")
add_analog("P_RampDNRANGE","REAL",                   PV_DESC="Ramping DOWN Range",                       PV_EGU="%")

#Locking mechanism
add_analog("P_Faceplate_LockID","DINT",              PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID","DINT",              PV_DESC="Device ID after Blockicon Open")
