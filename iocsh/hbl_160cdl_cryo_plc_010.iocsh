
#- @field IPADDR
#- @runtime YES
#- PLC IP address

#- @field RECVTIMEOUT
#- @type INTEGER
#- PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)

#- @field DBDIR
#- @runtime YES
#- The directory where the db files are located

#- @field MODVERSION
#- @runtime YES
#- The version of the PLC-IOC integration

#- @field hbl_160cdl_cryo_plc_010_VERSION
#- @runtime YES

#- @field S7_PORT
#- @runtime YES
#- Can override S7 port with this

#- @field MB_PORT
#- @runtime YES
#- Can override Modbus port with this

#-
#- Check if MODVERSION is set
#-
#- First set PLCIOCVERSION to a safe default; the module version if it is a module else the creation date
epicsEnvSet("PLCIOCVERSION", "$(hbl_160cdl_cryo_plc_010_VERSION=20241122220316)")
#- Now, the tricky part;
#- 1. try to set PLCIOCVERSION from a macro named PLCIOCVERSION + MODVERSION (where MODVERSION defaults to the empty string if not set)
#-    this will basically set PLCIOCVERSION to the value of PLCIOCVERSION if MODVERSION is not set or empty
#- 2. if MODVERSION _is_ set to a non empty string then PLCIOCVERSION will be set to the value of MODVERSION because
#-    the constructed macro name (from the macros PLCIOCVERSION + MODVERSION) will not exist and the value of MODVERSION will be used as a default
epicsEnvSet("PLCIOCVERSION", "$(PLCIOCVERSION$(MODVERSION=)=$(MODVERSION))")

#- S7 port           : 2000
#- Input block size  : 17342 bytes
#- Output block size : 0 bytes
#- Endianness        : BigEndian
s7plcConfigure("HBL-160CDL:Cryo-PLC-010", $(IPADDR=cds-plc-hbl-160.tn.esss.lu.se), $(S7_PORT=2000), 17342, 0, 1, $(RECVTIMEOUT=300), 0)

#- Modbus port       : 502
drvAsynIPPortConfigure("HBL-160CDL:Cryo-PLC-010", $(IPADDR=cds-plc-hbl-160.tn.esss.lu.se):$(MB_PORT=502), 0, 0, 1)

#- Link type         : TCP/IP (0)
#- The timeout is initialized to the (modbus) default if not specified
modbusInterposeConfig("HBL-160CDL:Cryo-PLC-010", 0, $(RECVTIMEOUT=0), 0)

#- Slave address     : 0
#- Function code     : 16 - Write Multiple Registers
#- Addressing        : Absolute (-1)
#- Data segment      : 20 words
drvModbusAsynConfigure("HBL-160CDL:Cryo-PLC-010write", "HBL-160CDL:Cryo-PLC-010", 0, 16, -1, 20, 0, 0, "S7-1500")

#- Slave address     : 0
#- Function code     : 3 - Read Multiple Registers
#- Addressing        : Relative (0)
#- Data segment      : 10 words
#- Polling           : 1000 msec
drvModbusAsynConfigure("HBL-160CDL:Cryo-PLC-010read", "HBL-160CDL:Cryo-PLC-010", 0, 3, 0, 10, 0, 1000, "S7-1500")

#- Load plc interface database
dbLoadRecords("$(DBDIR=)hbl_160cdl_cryo_plc_010.db", "PLCNAME=HBL-160CDL:Cryo-PLC-010, MODVERSION=$(PLCIOCVERSION), S7_PORT=$(S7_PORT=2000), MODBUS_PORT=$(MB_PORT=502), PAYLOAD_SIZE=17342")
#- Remove PLCIOCVERSION to not pollute the environment
epicsEnvUnset("PLCIOCVERSION")
